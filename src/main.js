import 'core-js/stable'
import Vue from 'vue'
import App from './App'
import router from './router'
import CoreuiVue from '@coreui/vue'
import { iconsSet as icons } from './assets/icons/icons.js'
import store from './store/index'
import axios from 'axios'
import VueAxios from 'vue-axios';
import VueRouter from 'vue-router';
import VueWait from 'vue-wait'
import Cookies from 'js-cookie';

Vue.config.performance = true
Vue.use(CoreuiVue)
Vue.prototype.$log = console.log.bind(console)
Vue.use(VueWait);
Vue.use(VueAxios, axios);
Vue.router = router;
App.router = Vue.router;
Vue.use(VueRouter);
//axios.defaults.headers.common['Authorization'] = Cookies.get('auth-token');

new Vue({
  el: '#app',
  router,
  store,
  wait: new VueWait(),
  icons,
  template: '<App/>',
  components: {
    App
  }
})
