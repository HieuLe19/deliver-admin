export default [
  {
    _name: 'CSidebarNav',
    _children: [
      {
        _name: 'CSidebarNavItem',
        name: 'Dashboard',
        to: '/dashboard',
        icon: 'cil-speedometer',
        badge: {
          color: 'primary',
          text: 'NEW'
        }
      },
      {
        _name: 'CSidebarNavDropdown',
        name: 'Users',
        route: '/users',
        icon: 'cil-user',
        items: [
          {
            name: 'List',
            to: '/users'
          },
          {
            name: 'Create',
            to: '/users/create'
          },
        ]
      },
      {
        _name: 'CSidebarNavDropdown',
        name: 'Notifications',
        route: '/notifications',
        icon: 'cil-bell',
        items: [
          {
            name: 'List',
            to: '/notifications'
          },
          {
            name: 'Create',
            to: '/notifications/create'
          },
        ]
      },
      {
        _name: 'CSidebarNavDropdown',
        name: 'Voucher',
        route: '/vouchers',
        icon: 'cil-bell',
        items: [
          {
            name: 'List',
            to: '/vouchers'
          },
          {
            name: 'Create',
            to: '/vouchers/create'
          },
        ]
      },
      {
        _name: 'CSidebarNavDropdown',
        name: 'Bills',
        route: '/Bills',
        icon: 'cil-basket',
        items: [
          {
            name: 'Create Bill',
            to: '/bills/create'
          },
          {
            name: 'Bills',
            to: '/bills'
          },
        ]
      },
      {
        _name: 'CSidebarNavDropdown',
        name: 'Master Settings',
        route: '/settings',
        icon: 'cil-settings',
        items: [
          {
            name: 'Settings',
            to: '/settings'
          },
        ]
      },
    ]
  }
]