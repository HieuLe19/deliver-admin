import Vue from 'vue'
import Router from 'vue-router'

// Containers
const TheContainer = () => import('@/containers/TheContainer')

// Views
const Dashboard = () => import('@/views/Dashboard')

const Colors = () => import('@/views/theme/Colors')
const Typography = () => import('@/views/theme/Typography')

const Charts = () => import('@/views/charts/Charts')
const Widgets = () => import('@/views/widgets/Widgets')

// Views - Components
const Cards = () => import('@/views/base/Cards')
const Forms = () => import('@/views/base/Forms')
const Switches = () => import('@/views/base/Switches')
const Tables = () => import('@/views/base/Tables')
const Tabs = () => import('@/views/base/Tabs')
const Breadcrumbs = () => import('@/views/base/Breadcrumbs')
const Carousels = () => import('@/views/base/Carousels')
const Collapses = () => import('@/views/base/Collapses')
const Jumbotrons = () => import('@/views/base/Jumbotrons')
const ListGroups = () => import('@/views/base/ListGroups')
const Navs = () => import('@/views/base/Navs')
const Navbars = () => import('@/views/base/Navbars')
const Paginations = () => import('@/views/base/Paginations')
const Popovers = () => import('@/views/base/Popovers')
const ProgressBars = () => import('@/views/base/ProgressBars')
const Tooltips = () => import('@/views/base/Tooltips')

// Views - Buttons
const StandardButtons = () => import('@/views/buttons/StandardButtons')
const ButtonGroups = () => import('@/views/buttons/ButtonGroups')
const Dropdowns = () => import('@/views/buttons/Dropdowns')
const BrandButtons = () => import('@/views/buttons/BrandButtons')

// Views - Icons
const CoreUIIcons = () => import('@/views/icons/CoreUIIcons')
const Brands = () => import('@/views/icons/Brands')
const Flags = () => import('@/views/icons/Flags')

// Views - examples
const Alerts = () => import('@/views/examples/Alerts')
const Badges = () => import('@/views/examples/Badges')
const Modals = () => import('@/views/examples/Modals')

// Views - Pages
const Page404 = () => import('@/views/pages/Page404')
const Page500 = () => import('@/views/pages/Page500')
const Login = () => import('@/views/pages/Login')
const Register = () => import('@/views/pages/Register')

// Users
const Users = () => import('@/views/users/Users')
const User = () => import('@/views/users/User')
const CreateUser = () => import('@/views/users/CreateUser')
const UserChart = () => import('@/views/users/UserChart')

// Views - Notifications
const Notifications = () => import('@/views/notifications/Notifications')
const Notification = () => import('@/views/notifications/Notification')
const CreateNotification = () => import('@/views/notifications/CreateNotification')

// Views - Vouchers
const Vouchers = () => import('@/views/vouchers/Vouchers')
const Voucher = () => import('@/views/vouchers/Voucher')
const CreateVoucher = () => import('@/views/vouchers/Create')

//Views - Bill
const CreateBill = () => import('@/views/Bill/Create')
const Bills = () => import('@/views/Bill/Bills')

//Views - Master Setting
const MasterSetting = () => import('@/views/master-settings/');

Vue.use(Router)

export default new Router({
  mode: 'hash', // https://router.vuejs.org/api/#mode
  linkActiveClass: 'active',
  scrollBehavior: () => ({ y: 0 }),
  routes: configRoutes()
})

function configRoutes() {
  return [
    {
      path: '/',
      redirect: '/dashboard',
      name: 'Home',
      component: TheContainer,
      children: [
        {
          path: 'dashboard',
          name: 'Dashboard',
          component: Dashboard
        },
        {
          path: 'theme',
          redirect: '/theme/colors',
          name: 'Theme',
          component: {
            render(c) { return c('router-view') }
          },
          children: [
            {
              path: 'colors',
              name: 'Colors',
              component: Colors
            },
            {
              path: 'typography',
              name: 'Typography',
              component: Typography
            }
          ]
        },
        {
          path: 'charts',
          name: 'Charts',
          component: Charts
        },
        {
          path: 'widgets',
          name: 'Widgets',
          component: Widgets
        },
        {
          path: 'users',
          redirect: '/users/list',
          meta: {
            label: 'Users'
          },
          component: {
            render(c) {
              return c('router-view')
            }
          },
          children: [
            {
              path: '',
              name: 'Users',
              component: Users
            },
            {
              path: 'create',
              name: 'Create User',
              component: CreateUser
            },
            {
              path: ':id/chart',
              meta: {
                label: 'User Chart'
              },
              name: 'UserChart',
              component: UserChart
            },
            {
              path: ':id',
              meta: {
                label: 'User Details'
              },
              name: 'User',
              component: User
            },
          ]
        },
        // {
        //   path: 'examples',
        //   redirect: '/examples/alerts',
        //   name: 'examples',
        //   component: {
        //     render(c) { return c('router-view') }
        //   },
        //   children: [
        //     {
        //       path: 'alerts',
        //       name: 'Alerts',
        //       component: Alerts
        //     },
        //     {
        //       path: 'badges',
        //       name: 'Badges',
        //       component: Badges
        //     },
        //     {
        //       path: 'modals',
        //       name: 'Modals',
        //       component: Modals
        //     }
        //   ]
        // },
        {
          path: '/notifications',
          redirect: '/pages/404',
          name: 'Notifications',
          component: {
            render(c) { return c('router-view') }
          },
          children: [
            {
              path: 'create',
              name: 'CreateNotification',
              component: CreateNotification
            },
            {
              path: '',
              name: 'List',
              component: Notifications
            },
            {
              path: ':id',
              meta: {
                label: 'Notification Detail'
              },
              name: 'Notification',
              component: Notification
            },
          ]
        },
        {
          path: '/vouchers',
          redirect: '/pages/404',
          name: 'Vouchers',
          component: {
            render(c) { return c('router-view') }
          },
          children: [
            {
              path: '',
              name: 'List',
              component: Vouchers
            },
            {
              path: ':id',
              meta: {
                label: 'Voucher Detail'
              },
              name: 'Voucher',
              component: Voucher
            },
            {
              path: 'create',
              name: 'Create',
              component: CreateVoucher
            },
          ]
        },
        {
          path: '/bills',
          redirect: '/pages/404',
          name: 'MasterSetting',
          component: {
            render(c) { return c('router-view') }
          },
          children: [
            {
              path: 'create',
              name: 'Create',
              component: CreateBill
            },
            {
              path: '',
              name: 'List',
              component: Bills
            },
          ]
        },
        {
          path: '/settings',
          redirect: '/pages/404',
          name: 'MasterSetting',
          component: {
            render(c) { return c('router-view') }
          },
          children: [
            {
              path: '',
              name: 'List',
              component: MasterSetting
            },
          ]
        }
      ]
    },
    {
      path: '/pages',
      redirect: '/pages/404',
      name: 'Pages',
      component: {
        render(c) { return c('router-view') }
      },
      children: [
        {
          path: '404',
          name: 'Page404',
          component: Page404
        },
        {
          path: '500',
          name: 'Page500',
          component: Page500
        }
      ]
    },
    {
      path: '/login',
      name: 'Login',
      component: Login
    },
    {
      path: '/register',
      name: 'Register',
      component: Register
    }
  ]
}