export const USER_OPTION = [
    { value: 0, label: "All" },
    { value: 1, label: "Online User" },
    { value: 2, label: "Offline User" },
    { value: 3, label: "Banned User" },
];