import axios from 'axios'
import store from './../store'
import Cookies from 'js-cookie'

// Add a request interceptor
axios.interceptors.request.use(function (config) {
    // Do something before request is sent
    return config;
}, function (error) {
    // Do something with request error
    return Promise.reject(error);
});

// Add a response interceptor
axios.interceptors.response.use((response) => {
    // Do something with response data
    return response;
}, (error) => {
    // Do something with response error
    if (error && error.response && error.response.data && error.response.data.message === "authorization error") {
        Cookies.set('logout', true);
    }
    return Promise.reject(error);
});

export default axios;