import Vue from 'vue';
import Vuex from 'vuex';
import users from './modules/users';
import user from './modules/user';
Vue.use(Vuex)

const state = {
  sidebarShow: 'responsive',
  sidebarMinimize: false
}

const mutations = {
  toggleSidebarDesktop(state) {
    const sidebarOpened = [true, 'responsive'].includes(state.sidebarShow)
    state.sidebarShow = sidebarOpened ? false : 'responsive'
  },
  SET_USER_LIST(state, users) {
    state.userList = users;
  },
  toggleSidebarMobile(state) {
    const sidebarClosed = [false, 'responsive'].includes(state.sidebarShow)
    state.sidebarShow = sidebarClosed ? true : 'responsive'
  },
  set(state, [variable, value]) {
    state[variable] = value
  }
}
const modules = {
  users,
  user
}


export default new Vuex.Store({
  state,
  modules,
  mutations
})