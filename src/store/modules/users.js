export default {
    state: {
        userList: {}
    },
    getters: {
        userList: state => state.userList
    },
    mutations: {
        SET_USER_LIST(state, users) {
            state.userList = users;
        }
    },
    actions: {
        setUserList({ commit }, data) {
            commit('SET_USER_LIST', data);
        },
    }
}
