export default {
    state: {
        userDetails: {}
    },
    getters: {
        userDetails: state => state.userDetails
    },
    mutations: {
        SET_USER_DETAIL(state, user) {
            state.userDetails = user;
        }
    },
    actions: {
        setUserDetail({ commit }, data) {
            console.log('DISPATCH USER DETAIL', data);
            commit('SET_USER_DETAIL', data);
        },
    }
}
