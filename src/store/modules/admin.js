export default {
    state: {
        adminInfo: {}
    },
    getters: {
        adminInfo: state => state.adminInfo
    },
    mutations: {
        SET_ADMIN_INFO(state, admin) {
            state.adminInfo = admin;
        }
    },
    actions: {
        setAdminInfo({ commit }, data) {
            commit('SET_ADMIN_INFO', data);
        },
        clearCookies({ commit }) {
            var cookies = document.cookie.split(";");

            for (var i = 0; i < cookies.length; i++) {
                var cookie = cookies[i];
                var eqPos = cookie.indexOf("=");
                var name = eqPos > -1 ? cookie.substr(0, eqPos) : cookie;
                document.cookie = name + "=;expires=Thu, 01 Jan 1970 00:00:00 GMT";
            }
            commit('SET_ADMIN_INFO', {});
        }
    }
}
