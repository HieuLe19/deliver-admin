module.exports = {
  lintOnSave: false,
  runtimeCompiler: true,
  configureWebpack: {
    //Necessary to run npm link https://webpack.js.org/configuration/resolve/#resolve-symlinks
    // NODE_ENV: '"development"',
    // SERVER_URI: "http://127.0.0.1:8000",
    resolve: {
      symlinks: false
    }
  },
  transpileDependencies: [
    '@coreui/utils'
  ]
}
